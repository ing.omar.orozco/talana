Django==3.0.5
django-allauth==0.41.0
django-extensions==2.2.9
django-jet==1.0.8
django-rest-auth==0.9.5
djangorestframework==3.11.0
